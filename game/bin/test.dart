import 'dart:io';

import 'test.dart';
import 'Player.dart';
import 'Pile.dart';
import 'game.dart';
import 'Card.dart';

void main(List<String> arguments) {
  Game g = Game();
  Pile p = Pile();
  // List<Card> c1;

  g.showWelcome();

  //nameOfplayer

  print("Player1 please input your name : ");
  Player p1 = Player(stdin.readLineSync()!, 0, "Player1");
  print("Player2 please input your name : ");
  Player p2 = Player(stdin.readLineSync()!, 0, "Player2");

  g.showRule(); //show rule of game

  g.showRound(); //show round current

  void afd() {
    switch (g.getCheckP()) {
      case 1: //player1:Y player2:Y
        g.askForDig(p1.getName(), p1.getStatus());
        g.askForDig(p2.getName(), p2.getStatus());
        break;
      case 2: //player1:Y player2:N
        g.askForDig(p1.getName(), p1.getStatus());
        break;
      case 3: //player1:N player2:Y
        g.askForDig(p2.getName(), p2.getStatus());
        break;
      default:
        break;
    }

    g.askForOpenCard();
  }

  afd();
  //นี้

  void pCard(List<Card> c1) {
    int Stone = 0, Snake = 0, Fire = 0;
    for (int i = 0; i < g.getCount(); i++) {
      String crName = c1[i].getName();
      int crNum = c1[i].getScore();

      switch (i) {
        case 0:
          print("The Card is : $crName   Number :  $crNum");
          //print("case1");
          if (crName == "Stone") {
            Stone++;
          } else if (crName == "Snake") {
            Snake++;
          } else if (crName == "Fire") {
            Fire++;
          }

          g.cardCurrent(crNum, Stone, Snake, Fire);
          afd();
          if (g.getNum() == 1) {
            break;
          } else {
            continue;
          }

        case 1:
          print("The Card is : $crName   Number :  $crNum");
          //print("case2");
          if (crName == "Stone") {
            Stone++;
          } else if (crName == "Snake") {
            Snake++;
          } else if (crName == "Fire") {
            Fire++;
          }

          g.cardCurrent(crNum, Stone, Snake, Fire);
          afd();
          if (g.getNum() == 1) {
            break;
          } else {
            continue;
          }

        case 2:
          print("The Card is : $crName   Number :  $crNum");
          //print("case3");
          if (crName == "Stone") {
            Stone++;
          } else if (crName == "Snake") {
            Snake++;
          } else if (crName == "Fire") {
            Fire++;
          }

          g.cardCurrent(crNum, Stone, Snake, Fire);
          afd();
          if (g.getNum() == 1) {
            break;
          } else {
            continue;
          }

        case 3:
          print("The Card is : $crName   Number :  $crNum");
          //print("case4");
          if (crName == "Stone") {
            Stone++;
            print("hello");
          } else if (crName == "Snake") {
            Snake++;
          } else if (crName == "Fire") {
            Fire++;
          }

          g.cardCurrent(crNum, Stone, Snake, Fire);
          afd();
          if (g.getNum() == 1) {
            break;
          } else {
            continue;
          }

        case 4:
          print("The Card is : $crName   Number :  $crNum");
          //print("case5");
          if (crName == "Stone") {
            Stone++;
          } else if (crName == "Snake") {
            Snake++;
          } else if (crName == "Fire") {
            Fire++;
          }

          g.cardCurrent(crNum, Stone, Snake, Fire);
          afd();
          if (g.getNum() == 1) {
            break;
          } else {
            continue;
          }

        case 5:
          print("The Card is : $crName   Number :  $crNum");
          //print("case6");
          if (crName == "Stone") {
            Stone++;
          } else if (crName == "Snake") {
            Snake++;
          } else if (crName == "Fire") {
            Fire++;
          }

          g.cardCurrent(crNum, Stone, Snake, Fire);
          afd();
          if (g.getNum() == 1) {
            break;
          } else {
            continue;
          }

        case 6:
          print("The Card is : $crName   Number :  $crNum");
          //print("case7");
          if (crName == "Stone") {
            Stone++;
          } else if (crName == "Snake") {
            Snake++;
          } else if (crName == "Fire") {
            Fire++;
          }

          g.cardCurrent(crNum, Stone, Snake, Fire);
          afd();
          if (g.getNum() == 1) {
            break;
          } else {
            continue;
          }

        case 7:
          print("The Card is : $crName   Number :  $crNum");
          //print("case8");
          print(g.getCount());
          if (crName == "Stone") {
            Stone++;
          } else if (crName == "Snake") {
            Snake++;
          } else if (crName == "Fire") {
            Fire++;
          }

          g.cardCurrent(crNum, Stone, Snake, Fire);
          afd();
          if (g.getNum() == 1) {
            break;
          } else {
            continue;
          }

        case 8:
          print("The Card is : $crName   Number :  $crNum");
          //print("case9");
          print(g.getCount());
          if (crName == "Stone") {
            Stone++;
          } else if (crName == "Snake") {
            Snake++;
          } else if (crName == "Fire") {
            Fire++;
          }

          g.cardCurrent(crNum, Stone, Snake, Fire);
          afd();
          if (g.getNum() == 1) {
            break;
          } else {
            continue;
          }

        case 9:
          print("The Card is : $crName   Number :  $crNum");
          //print("case10");
          print(g.getCount());
          if (crName == "Stone") {
            Stone++;
          } else if (crName == "Snake") {
            Snake++;
          } else if (crName == "Fire") {
            Fire++;
          }

          g.cardCurrent(crNum, Stone, Snake, Fire);
          print("**cave collapse**");
          g.showScore();
          break;
        default:
      }
    }
  }

  List<Card> c1 = [];

  c1 = [
    new Card("Diamond", 22),
    new Card("Gem", 5),
    new Card("Stone", 0),
    new Card("Diamond", 22),
    new Card("Gold", 10),
    new Card("snake", 0),
    new Card("Gem", 5),
    new Card("Gem", 5),
    new Card("Fire", 0),
    new Card("Fire", 0),
  ];

  pCard(c1);
}