class Player{
  late String name;
  late int score;
  late String status;

Player(this.name,this.score,this.status);

String getStatus(){
  return status;
}
String getName(){
  return name;
}

int getScore(){
  return score;
}
}