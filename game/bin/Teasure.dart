import 'Card.dart';

abstract class Treasure implements Card {
  late String name;
  late int number;

  Treasure(this.name, this.number);

  @override
  String getName() {
    return name;
  }

  int getNumber() {
    return number;
  }

  @override
  void setName(String name){
    this.name=name;
  }
  
  void setNumber(int number){
    this.number=number;
  }
  

}