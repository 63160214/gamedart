import 'dart:io';
import 'Teasure.dart';
import 'Card.dart';
//import 'Pile.dart';
import 'Player.dart';
import 'pile.dart';

class Game {
  late Player p1, p2;
  late int round = 1;
  String name = "";
  String nameP1 = "";
  String nameP2 = "";
  late String ask;
  int open1 = 1;
  int open2 = 1;
  late String ask1;
  late String ask2;
  late List<Treasure> T;
  late Card c;
  int Stone = 0, Snake = 0, Fire = 0;
  List listPlayer1 = [];
  List listPlayer2 = [];
  int count = 0;
  int num = 0;
  int checkP = 1;
  int pointP1 = 0;
  int pointP2 = 0;

  int AllPointP1 = 0;
  int AllPointP2 = 0;

  void setStone(int Stone) {
    this.Stone = Stone;
  }

  void setSnake(int Snake) {
    this.Snake = Snake;
  }

  void setFire(int Fire) {
    this.Fire = Fire;
  }

  int getSnake() {
    return Snake;
  }

  int getStone() {
    return Stone;
  }

  int getFire() {
    return Fire;
  }

  int getCount() {
    return count;
  }

  int getNum() {
    return num;
  }

  int getCheckP() {
    return checkP;
  }

  int getPointP1() {
    return pointP1;
  }

  int getPointP2() {
    return pointP2;
  }

  int getAllPointP1() {
    return AllPointP1;
  }

  int getAllPointP2() {
    return AllPointP2;
  }
  // List<Card> cardPile1 = p.getpile1();

  
  void showRound() {
    print(":: Round $round ::");
    round++;
  }

  void askForDig(String name, String status) {
    print("$name($status) Will dig a cave this time or not? (Y/N)");
    ask = stdin.readLineSync()!;
    //Player1
    if (status == "Player1") {
      nameP1 = name;
      if (ask == "Y") {
        open1 = 1;
      } else if (ask == "N") {
        open1 = 0;
      }

      //Player2
    } else if (status == "Player2") {
      nameP2 = name;
      if (ask == "Y") {
        open2 = 1;
      } else if (ask == "N") {
        open2 = 0;
      }
    }
  }

  void askForOpenCard() {
    openCard(open1, open2);
  }

  void showWelcome() {
    print("Welcome To Incan Gold !!");
  }

  void showRule() {
    print("-----------------------------------------------");
    print("|    We will play 1 round                     |");
    print("|    Whoever gets more Teasure points wins    |");
    print("-----------------------------------------------");
  }

  void openCard(int open1, int open2) {
    if (open1 == 1 && open2 == 1) {
      count++;

      checkP = 1;
      print("✦——————————✦ open a card from the pile ✦——————————✦\n");
    } else if (open1 == 1 && open2 == 0) {
      count++;

      checkP = 2;
      print("✦——————————✦ open a card from the pile ✦——————————✦\n");
    } else if (open1 == 0 && open2 == 1) {
      count++;

      checkP = 3;
      print("✦——————————✦ open a card from the pile ✦——————————✦\n");
    } else if (open1 == 0 && open2 == 0) {
      num = 1;
      print("₊˚✩‧₊˚cave collapse ₊˚✩‧₊˚");
      print("Point Player1 : $pointP1");
      print("Point Player2 : $pointP2");
      showScore();
      //checkWin(name);
    }
  }

  void showScore() {
    if (pointP1 > pointP2) {
      print("✧┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈✧");
      print("              $nameP1 WINNNNNN               ");
      print("✧┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈✧");
      print("Point $nameP1 : $pointP1");
      print("Point $nameP2 : $pointP2");
    } else {
      print("✧┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈✧");
      print("             $nameP2 WINNNNNN               ");
      print("✧┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈✧");
      print("Point $nameP1  : $pointP1");
      print("Point $nameP2: $pointP2");
    }
  }

  int sumTeasure = 0;

  void cardCurrent(int crNum, int Stone, int snake, int Fire) {
    sumTeasure = sumTeasure + crNum;

    switch (checkP) {
      case 1:
        pointP1 = sumTeasure;
        pointP2 = sumTeasure;
        break;
      case 2:
        pointP1 = sumTeasure;
        break;
      case 3:
        pointP2 = sumTeasure;
        break;
    }
    //print("╔══════════════════════════════════════════════════════════════════════════════════╗");
    //print("   Teasure point $sumTeasure , Barrier card (Stone=$Stone,Snake=$Snake,Fire=$Fire)  ");
    //print("╚══════════════════════════════════════════════════════════════════════════════════╝");
    print("Point Player1 : $pointP1");
    print("Point Player2 : $pointP2");
    print("------------------------------------------------------");
  }
}